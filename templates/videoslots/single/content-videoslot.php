<?php

$terms = get_the_terms($post->ID, 'software');

foreach ($terms as $term) {
    $term_num = $term->term_id;
    $term_name = $term->name;
}

?>

<div class="single-videoslot-content aic-container">
    <h1 class="custom-header"><?php the_title(); ?></h1>
    <div class="page-content">
        <div class="videoslot-upper">
            <div class="videoslot-bg">
                <div class="videoslots-iframe">
                    <iframe id="aic-iframe" src="<?php the_field('aic_videoslots_iframe'); ?>" width="800" height="500" style="background-color: rgb(0, 0, 0);" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" wmode="opaque"></iframe>
                    <div class="videoslots-btn">
                        <a href="<?php the_field('aic_videoslot_redirect'); ?>">PLAY FOR REAL</a>
                    </div>
                    <div class="videoslots-other">
                        <?php echo do_shortcode("[iframe_videoslot tax={$term_num}]"); ?>
                    </div>
                </div>

            </div>
            <div class="videoslots-info">
                <div class="vs-wrap">
                    <h3>Slot details</h3>
                    <div class="videoslots-table">
                        <div class="table-left">
                            <div class="vs-row">
                                <span>Software</span>
                                <span><?php echo $term_name; ?></span>
                            </div>
                            <div class="vs-row"><span>Slot Type</span><span><?php the_field('aic_videoslot_slot_type'); ?></span></div>
                            <div class="vs-row"><span>Paylines</span><span><?php the_field('aic_videoslot_paylines'); ?></span></div>
                            <div class="vs-row"><span>Reels</span><span><?php the_field('aic_videoslot_reels'); ?></span></div>
                        </div>
                        <div class="table-right">
                            <div class="vs-row"><span>Min Coins Size</span><span><?php the_field('aic_videoslot_min_coins'); ?></span></div>
                            <div class="vs-row"><span>Max Coins Size</span><span><?php the_field('aic_videoslot_reels'); ?></span></div>
                            <div class="vs-row"><span>Jackpot</span><span><?php the_field('aic_videoslot_jackpot'); ?></span></div>
                            <div class="vs-row"><span>RTP</span><span><?php the_field('aic_videoslot_rtp'); ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="videoslot-lower">
            <?php the_content(); ?>
        </div>
        <div class="videoslot-related">
            <h3>Related Games</h3>
            <?php

            echo do_shortcode("[related_videoslot limit=7 tax='{$term_num}']");

            ?>
        </div>
    </div>
</div>