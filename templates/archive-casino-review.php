<?php get_header(); ?>

<div class="aic-container single-casino-review page-content">
    <?php if (get_field('review_archive_page_content', 'options')) : ?>

        <div class="casino-review-title">
            <h1><?php the_field('aic_archive_title', 'options') ?></h1>
        </div>
        <div class="breadcrumbs">
            <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
        </div>
        <div class="dropdown-pages" style="margin-bottom: 2rem;">
            <button class="dropdown-button">Choose your country</button>
            <div class="drop-down-content">
                <?php generate_review_content_pages(); ?>
            </div>
        </div>
        <?php the_field('review_archive_page_content', 'options'); ?>

    <?php endif; ?>
        <?php if (!is_front_page()) : ?>
            <?php include ALL_IN_CASINO_BASE_DIR . 'templates/casino-reviews/single/author-box.php'; ?>
        <?php endif; ?>
</div>

<?php get_footer(); ?>