<?php

//Details about casino + for shortcodes
$shortname = get_field('casino_shortname');
$type = $atts['type'];

$name = do_shortcode("[media data=fullname casino=$shortname type={$type}]");
$logo = do_shortcode("[media data=logo casino=$shortname type={$type}]");
$rating = do_shortcode("[media data=rating casino=$shortname type={$type}]");
$ribbon = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon type={$type}]");
$oneliner = do_shortcode("[media data=oneliner casino=$shortname type={$type}]");
$pro1 = do_shortcode("[media data=extra-fields casino=$shortname field=pro1 type={$type}]");
$pro2 = do_shortcode("[media data=extra-fields casino=$shortname field=pro2 type={$type}]");
$pro3 = do_shortcode("[media data=extra-fields casino=$shortname field=pro3 type={$type}]");
$slot_games = do_shortcode("[media data=extra-fields casino=$shortname field=slot_games type={$type}]");
$live_games = do_shortcode("[media data=extra-fields casino=$shortname field=live_games type={$type}]");
$rib_col = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon_color type={$type}]");
$bonus_code = do_shortcode("[media data=extra-fields casino=$shortname field=bonus-code type={$type}]");

//Trackers(affiliate links)
$uk_tracker = do_shortcode("[media data=tracker casino=$shortname type={$type}]");
$ca_tracker = do_shortcode("[media data=tracker casino=$shortname tracker=ca type={$type}]");

//If tnc enabled
$tnc_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_text_enabled type={$type}]");
$tnc = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions type={$type}]");

//If tnc link enabled
$tnc_link = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_condition_link_text type={$type}]");
$tnc_link_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_link_enabled type={$type}]");

?>

<div class="top-three-single">
    <div class="element-1">
        <span style="background-color:<?php echo $rib_col; ?>"><?php echo "#" . $num; ?></span>
    </div>
    <div class="element-2">
        <?php if ($logo) : ?>
            <div class="logo">
                <img src="<?php echo $logo; ?>" alt="<?php echo $name . ' logo'; ?>">
            </div>
        <?php endif; ?>
        <?php if (strlen($name) != 57) : ?>
            <div class="casino-name">
                <?php echo $name; ?>
            </div>
        <?php endif; ?>
    </div>
    <!-- MOBILE ROW -->
    <div class="mobile-row">
        <div class="element-3">
            <div class="bonus">
                <?php echo $oneliner; ?>
            </div>
            <?php if (strlen($ribbon) > 3) : ?>
                <div class="label" style="background-color:<?php echo $rib_col; ?>">
                    <?= $ribbon; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="element-4">
            <?php if (!empty($pro1) && (strlen($pro1) > 3)) {
            ?>
                <span> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"></span> <?php echo $pro1; ?></span>
            <?php
            } ?>
            <?php if (!empty($pro2) && (strlen($pro2) > 3)) {
            ?>
                <span> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"></span> <?php echo $pro2; ?></span>
            <?php
            } ?>
            <?php if (!empty($pro3) && (strlen($pro3) > 3)) {
            ?>
                <span> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"></span> <?php echo $pro3; ?></span>
            <?php
            } ?>
        </div>
        <div class="element-5">
            <div class="first-row">
                <?php
                echo "<span>" . $slot_games . "</span>";
                ?>
            </div>
            <div class="second-row"><?php _e('Slot games', 'all-in-casino'); ?></div>
            <div class="third-row"><span><?php echo $live_games; ?></span><?php _e('Live games', 'all-in-casino'); ?></div>
        </div>
    </div>
    <!-- END MOBILE ROW -->
    <div class="element-3 desktop">
        <div class="bonus">
            <?php echo $oneliner; ?>
        </div>
        <?php if (strlen($ribbon) > 3) : ?>
            <div class="label" style="background-color:<?php echo $rib_col; ?>">
                <?= $ribbon; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="element-4 desktop">
        <?php if (!empty($pro1) && (strlen($pro1) > 3)) {
        ?>
            <div class='plus'> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"><?php echo $pro1; ?></div>
        <?php
        } ?>
        <?php if (!empty($pro2) && (strlen($pro2) > 3)) {
        ?>
            <div class='plus'> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"><?php echo $pro2; ?></div>
        <?php
        } ?>
        <?php if (!empty($pro3) && (strlen($pro3) > 3)) {
        ?>
            <div class='plus'> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" alt="tick"><?php echo $pro3; ?></div>
        <?php
        } ?>
    </div>
    <div class="element-5 desktop">
        <div class="first-row">
            <?php
            echo "<span>" . $slot_games . "</span>";
            ?>
        </div>
        <div class="second-row"><?php _e('Slot games', 'all-in-casino'); ?></div>
        <div class="third-row"><span><?php echo $live_games; ?></span><?php _e('Live games', 'all-in-casino'); ?></div>
    </div>

    <div class="element-rating">
        <div class="rating-image">
            <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/betspin-logo-ratings.png'; ?>" alt="rating-logo">
        </div>
        <div class="rating-value">
            <?php if (!empty($rating)) : ?>
                <?php echo $rating; ?>
            <?php else : ?>
                <?php echo "4.5"; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="element-6">
        <div class="cta-btn">
            <div class="btn-left">
                <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/explore.png' ?>" alt="explore">
            </div>
            <div class="btn-right">
                <?php if ($atts['country'] == 'canada') : ?>
                    <a href="<?php echo $ca_tracker; ?>" target="_blank"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
                <?php else : ?>
                    <a href="<?php echo $uk_tracker; ?>" target="_blank"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="element-7">
        <a href="<?php the_permalink(); ?>"><?php the_title("", " Review", true); ?></a>
    </div>
    <?php if ($tnc_enabled == '1') : ?>
        <p class="review-terms"><?php echo $tnc; ?> <?php if ($tnc_link_enabled == '1') : ?> <a href="<?php echo $tnc_link; ?>">T&C Apply</a><?php endif; ?></p>
    <?php endif; ?>
</div>