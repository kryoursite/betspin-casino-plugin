<?php

//Details about casino + for shortcodes
$shortname = get_field('casino_shortname');
$name = do_shortcode("[media data=fullname casino=$shortname]");
$logo = do_shortcode("[media data=logo casino=$shortname type=livecasino]");
$ribbon = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon type=livecasino]");
$oneliner = do_shortcode("[media data=oneliner casino=$shortname type=livecasino]");
$pro1 = do_shortcode("[media data=extra-fields casino=$shortname field=pro1 type=livecasino]");
$pro2 = do_shortcode("[media data=extra-fields casino=$shortname field=pro2 type=livecasino]");
$pro3 = do_shortcode("[media data=extra-fields casino=$shortname field=pro3 type=livecasino]");
$slot_games = do_shortcode("[media data=extra-fields casino=$shortname field=slot_games type=livecasino]");
$live_games = do_shortcode("[media data=extra-fields casino=$shortname field=live_games type=livecasino]");
$rib_col = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon_color type=livecasino]");
$bonus_code = do_shortcode("[media data=extra-fields casino=$shortname field=bonus-code type=livecasino]");

//Trackers(affiliate links)
$uk_tracker = do_shortcode("[media data=tracker casino=$shortname type=livecasino]");
$ca_tracker = do_shortcode("[media data=tracker casino=$shortname tracker=ca type=livecasino]");

//If tnc enabled
$tnc_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_text_enabled type=livecasino]");
$tnc = do_shortcode("[media data=extra-fields casino=$shortname field=tnc-text type=livecasino]");

//If tnc link enabled
$tnc_link = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_condition_link_text type=livecasino]");
$tnc_link_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_link_enabled type=livecasino]");
$cas_id = get_the_ID();

?>
<div class="box-single">
    <div class="box-logo">
        <?php if ($logo) : ?>
            <img src="<?php echo $logo; ?>" alt="<?php echo $name . ' logo'; ?>">
        <?php endif; ?>
    </div>

    <?php if ($cas_id == 47) : ?>
        <a class="box-bonus" href="<?php echo $uk_tracker; ?>" target="_blank">
            <?php
            if ($oneliner) {
                echo "<div class='box-bonus-line-1'><img class='snowdrift' src='" . ALL_IN_CASINO_PLUGIN_URL . 'public/img/snowdrift.png' . "' alt='snowdrift'><img class='snowflake' src='" . ALL_IN_CASINO_PLUGIN_URL . 'public/img/snowflake.png' . "' alt='snowflake'>" . $oneliner . "</div>";
            }
            ?>
        </a>
    <?php else : ?>
        <div class="box-bonus">
            <?php
            if ($oneliner) {
                echo "<div class='box-bonus-line-1'><img class='snowdrift' src='" . ALL_IN_CASINO_PLUGIN_URL . 'public/img/snowdrift.png' . "' alt='snowdrift'><img class='snowflake' src='" . ALL_IN_CASINO_PLUGIN_URL . 'public/img/snowflake.png' . "' alt='snowflake'>" . $oneliner . "</div>";
            }
            ?>
        </div>
    <?php endif; ?>

    <div class="bonus-code-wrap">
        <span class="bonus-text">Bonus Code</span>
        <div class="bonus-code">

            <?php echo $bonus_code; ?>

        </div>
    </div>
    <?php if ($uk_approved) : ?>

        <div class="flag-approved">
            UK: Approved
        </div>

    <?php endif; ?>
    <div class="claim-btn">
        <?php if ($atts['country'] == 'canada') : ?>
            <a href="<?php echo $ca_tracker; ?>" target="_blank"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
        <?php else : ?>
            <a href="<?php echo $uk_tracker; ?>" target="_blank"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
        <?php endif; ?>
    </div>
    <?php if ($tnc_enabled == '1') : ?>
        <div class="box-terms">
            <p class="box-tnc-text"><?= $tnc; ?>
                <br>
                <a href="<?= $tnc_link; ?>" target="_blank">Full terms apply</a>
            </p>
            <span class="box-tnc">T&C Apply</span>
        </div>
    <?php endif; ?>
</div>