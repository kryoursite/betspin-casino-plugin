<?php

$terms = get_the_terms($post->ID, 'software');

foreach ($terms as $term) {
    $term_name = $term->name;
}

?>

<div class="iframe-vs-single">
    <div class="iframe-vs-img">
        <?php the_post_thumbnail(); ?>
    </div>
    <div class="iframe-vs-title">
        <?php the_title(); ?>
    </div>
    <div class="iframe-vs-software">
        <?php echo $term_name; ?>
    </div>
</div>