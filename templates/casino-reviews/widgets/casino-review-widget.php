<?php

$shortname = get_field('casino_shortname');

$name = do_shortcode("[media data=fullname casino=$shortname type=livecasino]");

$logo = do_shortcode("[media data=logo casino=$shortname type=livecasino]");

$tracker = do_shortcode("[media data=tracker casino=$shortname type=livecasino]");

$oneliner = do_shortcode("[media data=oneliner casino=$shortname type=livecasino]");

?>

<div class="casino-review-widget-wrap">
    <div class="casino-review-widget-image">
        <?php if ($logo) : ?>
            <a href="<?php echo $tracker; ?>"> <img src="<?php echo $logo; ?>" alt="<?php echo $name . ' logo'; ?>"></a>
        <?php endif; ?>
    </div>
    <div class="casino-review-widget-bonus">
        <?php if (strlen($tracker) != 57) : ?>
            <a href="<?php echo $tracker; ?>"><?php echo $name; ?></a>
        <?php endif; ?>
        <span><?php echo $oneliner ?></span>
    </div>
    <div class="review-cta">
        <?php if (strlen($tracker) != 57) : ?>
            <a href="<?php echo $tracker; ?>" target="_blank"> <span>&raquo</span></a>
        <?php endif; ?>
    </div>
</div>