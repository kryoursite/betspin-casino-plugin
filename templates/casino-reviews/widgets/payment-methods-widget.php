<?php if (have_rows('taxonomy_repeater', 'widget_' . $widget_id)) : ?>

    <?php while (have_rows('taxonomy_repeater', 'widget_' . $widget_id)) : the_row();
        $tax = get_sub_field('tax_payment_method');
        $page = get_sub_field('tax_page_redirect');
        $img = get_field('aic_tax_payment_img', 'payment-method_' . $tax->term_taxonomy_id);
        
        if ($tax) : ?>
            <div class="casino-review-widget-wrap">
                <div class="casino-review-widget-image">
                    <img src="<?php echo $img['url']; ?>" alt="<?php echo $tax->name . ' logo'; ?>">
                </div>
                <div class="casino-review-widget-bonus">
                    <a><?php echo esc_html($tax->name); ?></a>
                </div>
                <div class="review-cta">
                    <a href="<?php echo $page; ?>"><span>»</span></a>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>