<?php include_once ALL_IN_CASINO_BASE_DIR . 'includes/all-in-casino-hercules-data.php'; ?>

<?php if (get_field('aic_archive_page')) : ?>

    <div class="single-casino-review aic-container page-content with-sidebar">
        <div class="single-archive-content">
            <div class="casino-review-title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="breadcrumbs">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            </div>
            <div class="dropdown-pages" style="margin-bottom: 2rem;">
                <button class="dropdown-button">Choose your country</button>
                <div class="drop-down-content">
                    <?php generate_review_content_pages(); ?>
                </div>
            </div>
            <?php the_content(); ?>
            <?php if (!is_front_page()) : ?>
                <?php include ALL_IN_CASINO_BASE_DIR . 'templates/casino-reviews/single/author-box.php'; ?>
            <?php endif; ?>
        </div>
    </div>

<?php else : ?>

    <div class="single-casino-review aic-container with-sidebar">
        <div class="page-content">
            <div class="casino-review-title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="breadcrumbs">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            </div>
            <div class="casino-intro">
                <div class="casino-intro-left">
                    <div class="intro-block-welcome">
                        <div class="welcome-logo">
                            <?php if ($logo) : ?>
                                <img src="<?php echo $logo; ?>" alt="<?php echo $name . ' logo'; ?>">
                            <?php endif; ?>
                        </div>

                        <?php if (is_single('47')) : ?>
                            <div class="welcome-bonus-info">
                                <?php if ($oneliner) : ?>
                                    <a href="<?php echo $uk_tracker; ?>" target="_blank"><?php echo $oneliner; ?></a href="">
                                <?php endif; ?>
                            </div>
                        <?php else : ?>
                            <div class="welcome-bonus-info">
                                <?php if ($oneliner) : ?>
                                    <div><?php echo $oneliner; ?></div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="welcome-cta">
                            <div class="cta-btn">
                                <div class="btn-left">
                                    <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/explore.png' ?>" height="24" width="24" alt="img-explore">
                                </div>
                                <div class="btn-right">
                                    <a href="<?php echo $uk_tracker; ?>"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="welcome-approved">
                            <?php if ($uk_approved) : ?>
                                <?php echo 'UK: Approved'; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="casino-intro-center">
                    <div class="review-pluses">

                        <?php if (!empty($pro1) && (strlen($pro1) > 3)) {
                        ?>
                            <div class="pluses-container"> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" height="22" width="22" alt="tick"> <span><?php echo $pro1; ?></span></div>
                        <?php
                        } ?>

                        <?php if (!empty($pro2) && (strlen($pro2) > 3)) {
                        ?>
                            <div class="pluses-container"> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" height="22" width="22" alt="tick"> <span><?php echo $pro2; ?></span></div>
                        <?php
                        } ?>

                        <?php if (!empty($pro3) && (strlen($pro3) > 3)) {
                        ?>
                            <div class="pluses-container"> <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/tick.png'; ?>" height="22" width="22" alt="tick"> <span><?php echo $pro3; ?></span></div>
                        <?php
                        } ?>

                    </div>
                </div>
                <div class="casino-intro-right">

                    <div class="review-slot-games">
                        <div class="num-container">
                            <span><?php echo $slot_games; ?></span>
                        </div>
                        <span class="slot-games">Slot games</span>
                    </div>

                    <div class="review-live-games">
                        <div class="review-live-wrap">
                            <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/live-roulette.png' ?>" height="22" width="22" alt="live-roulette"><span><?php echo $live_games; ?></span>
                        </div>
                        <span class="live-games">live games</span>
                    </div>

                    <div class="review-bonus-code">
                        <span>Bonus Code</span>
                        <?php if (strlen($bonus_code) > 2) : ?>

                            <span><?php echo $bonus_code; ?></span>

                        <?php else : ?>
                            <span> - </span>
                        <?php endif; ?>

                    </div>
                </div>
                <?php if (is_single(47)) : ?>
                    <div class="casino-intro-bottom">
                        <?php the_field('bet365_t&c'); ?>
                        <a href="<?php the_field('bet365_redirect'); ?>">T&C Apply</a>
                    </div>
                <?php endif; ?>
            </div>
            <?php if (function_exists('betspin_ratings_row')  && is_plugin_active('media-back-office/media-back-office.php')) : ?>
                <div class="ratings-row">
                    <?php betspin_ratings_row(); ?>
                </div>
            <?php endif; ?>
            <div class="review-nav">
                <a href="#live-games">Casino Games</a>
                <a href="#live-bonuses">Casino Bonuses</a>
                <a href="#pros-cons">Pros and Cons</a>
                <a href="#banking-options">Banking Options</a>
                <a href="#how-to-signup">Sign Up</a>
                <a href="#mobile-live">Mobile Casino</a>
                <a href="#security">Security</a>
                <a href="#try-today">Try Today</a>
            </div>
            <div class="casino-intro-text">
                <h2 id="casino-intro-text" style="font-weight: bold;"><?php the_title(); ?> introduction</h2>
                <div class="intro-content">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="live-games">
                <h2 id="live-games"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/games.png'; ?>" height="20" width="20" alt="review games"> Live Casino Games</h2>
                <div class="live-games-content">
                    <?php the_field('betspin_live_casino_games'); ?>
                </div>
            </div>
            <div class="live-bonuses">
                <h2 id="live-bonuses"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/bonuses-chips.png'; ?>" height="20" width="20" alt="bonus chips"> Live Casino Bonuses </h2>
                <div class="live-bonuses-content">
                    <?php the_field('betspin_live_casino_bonuses'); ?>
                </div>
                <?php if (get_field('betspin_about_bonus_box')) : ?>
                    <div class="about-bonus-box">
                        <div class="about-bonus__text"><?php the_field('betspin_about_bonus_box'); ?></div>
                        <div class="cta-btn">
                            <div class="btn-left">
                                <img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/explore.png' ?>" height="24" width="24" alt="explore casinos">
                            </div>
                            <div class="btn-right">
                                <a href="<?php the_field('betspin_redirect_link'); ?>"> <i class="icon-right-open"></i> <?php _e('CLAIM BONUS', "all-in-casino"); ?></a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
    <div class="pros-cons">
        <h2 id="pros-cons"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/pros-cons.png'; ?>" height="20" width="20" alt="pros cons"> Pros and Cons </h2>
        <div class="pros-cons-content">
            <?php if (get_field('bb_pros_cons_field')) : ?>
                <div class="pros-cons-block">
                    <div class="pros-cons-content">
                        <?php the_field('bb_pros_cons_field'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="banking-options">
                <h2 id="banking-options"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/deposit.png'; ?>" height="20" width="20" alt="banking options"><?php the_title(); ?> Banking Options</h2>
                <div class="banking-content">
                    <?php the_field('betspin_banking_options'); ?>
                </div>
            </div>

            <div class="how-to-signup">
                <h2 id="how-to-signup"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/general-info.png'; ?>" height="20" width="20" alt="signup"> How to Sign Up</h2>
                <div class="signup-content">
                    <?php the_field('betspin_how_to_sign_up'); ?>
                </div>
            </div>

            <div class="mobile-live">
                <h2 id="mobile-live"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/quick-facts.png'; ?>" alt="mobile live casino"> <?php the_title(); ?> Mobile Live Casino</h2>
                <div class="mobile-live-content">
                    <?php the_field('betspin_mobile_live'); ?>
                </div>
            </div>

            <div class="security">
                <h2 id="security"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/support.png'; ?>" height="20" width="20" alt="security"> Security and Customer Support</h2>
                <div class="security-content">
                    <?php the_field('betspin_security'); ?>
                </div>
            </div>

            <div class="try-today">
                <h2 id="try-today"><img src="<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/live-casino.png'; ?>" height="20" width="20" alt="try today"> Try <?php the_title(); ?> Live Games Today</h2>
                <div class="try-today-content">
                    <?php the_field('betspin_live_games_today'); ?>
                </div>
            </div>

            <?php if (!get_field('disable_bb')) : ?>
                <div class="review-bottom-block" <?php
                                                    $col = get_field('bb_bg');
                                                    if (get_field('bb_bg')) : echo "style='background:{$col}'";
                                                    endif; ?>>
                    <div class="bottom-block-img">
                        <?php if ($logo) : ?>
                            <img src="<?php echo $logo; ?>" alt="<?php echo $name . ' logo'; ?>">
                        <?php endif; ?>
                    </div>
                    <span class="block-bonus-txt"><?php the_field('bb_bonus_text'); ?></span>
                    <a href="<?php echo $uk_tracker; ?>" class="bb-btn"> <i class="icon-right-open"></i> CLAIM BONUS</a>
                </div>
            <?php endif; ?>

            <?php if (get_field('betspin_faq_field')) : ?>
                <div class="faq-block">
                    <div class="faq-block-content">
                        <?php the_field('betspin_faq_field'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!is_front_page()) : ?>
                <?php include ALL_IN_CASINO_BASE_DIR . 'templates/casino-reviews/single/author-box.php'; ?>
            <?php endif; ?>

        </div>
        <aside class="sidebar">
            <?php

            dynamic_sidebar('sidebar');

            ?>
        </aside>
    </div>

<?php endif; ?>