(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.js-filter-item', function (e) {
            e.preventDefault();

            var category = $(this).data('category');

            $.ajax({
                url: wp_ajax.ajax_url,
                data: { action: 'filter', category: category },
                type: 'POST',
                beforeSend: function () {
                    $(".loader").css("display", "flex");
                    $(".simple-single").hide();
                    $(".casino-filter span").hide();
                },

                success: function (result) {
                    $('.js-filter').html(result);
                },

                complete: function (data) {
                    $(".loader").css("display", "none");
                },

                error: function (result) {
                    console.warn(result);
                }
            });
        });


        $(document).on('click', '.vs-js-filter-item', function (e) {
            e.preventDefault();

            var category = $(this).data('category');

            $.ajax({
                url: wp_ajax.ajax_url,
                data: { action: 'filter_vs_ajax', category: category },
                type: 'POST',
                beforeSend: function () {
                    $(".vs-loader").css("display", "flex");
                    $(".single-vs").hide();
                },

                success: function (result) {
                    $('.vs-js-filter').html(result);
                },

                complete: function (data) {
                    $(".vs-loader").css("display", "none");
                },

                error: function (result) {
                    console.warn(result);
                }
            });
        });
    });

})(jQuery);