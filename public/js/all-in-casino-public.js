jQuery(document).ready(function ($) {

	//Dropdown menu for review content pages
	$(".dropdown-button").on("click", function () {
		$(".drop-down-content").slideToggle();
	});

	//Casino review toplist - show payments tab
	$('.casino-reviews-list').on('click', '.more-tab-button', function () {
		var mainTab = $(this).parent().parent().siblings("#main-tab");
		var paymentsTab = $(this).parent().parent().siblings("#payments-tab");
		var mainTabBtn = $(this).siblings(".main-tab-button");

		mainTab.hide();
		paymentsTab.show();
		mainTabBtn.show();
	});

	//Casino review toplist - show main tab
	$('.casino-reviews-list').on('click', '.main-tab-button', function () {
		var mainTab = $(this).parent().parent().siblings("#main-tab");
		var paymentsTab = $(this).parent().parent().siblings("#payments-tab");
		var mainTabBtn = $(this);

		mainTab.show();
		paymentsTab.hide();
		mainTabBtn.hide();
	});


	//Load slot iframe with button click
	$(".load-iframe").click(function () {
		var iframe = $("#aic-iframe");
		iframe.attr("src", iframe.data("src"));
		$(".iframe-wrap").hide();
	});

	//Close featured casino window
	$(".fc-close-btn").click(function () {
		var bottomBar = $(".fc-sticky-bottom");
		bottomBar.fadeOut();
	});

	//Show videoslot overlay on hover
	$(document).on("mouseenter", '.main-vs', function () {
		$(this).children('.overlay-vs').css("display", "flex");
	});

	$(document).on("mouseleave", '.main-vs', function () {
		$(this).children('.overlay-vs').css("display", "none");
	});

	//Show TNC on box shortcode
	$(document).on("mouseenter", '.box-tnc', function () {
		$(this).siblings('.box-tnc-text').css("display", "block");
	});

	$(document).on("mouseleave", '.box-tnc', function () {
		$(this).siblings('.box-tnc-text').css("display", "none");
	});

	$('.box-tnc-text').on("mouseenter", function () {
		$(this).css("display", "block");
	});

	$('.box-tnc-text').on("mouseleave", function () {
		$(this).css("display", "none");
	});

	//Show videoslot overlay on hover
	$(document).on("mouseenter", '.disc-hover', function () {
		$(this).siblings('.disclosure-text').css("display", "block");
	});

	$(document).on("mouseleave", '.disc-hover', function () {
		$(this).siblings('.disclosure-text').css("display", "none");
	});
});

