
(function ($) {
    $(document).ready(function () {
      var toplistPage = 1;    

  $(".load-more-casinos").on("click", function (e) {
    e.preventDefault();
    toplistPage++;
    const button = $('.load-more-casinos');
    $.ajax({
        method: 'POST',
        url: wp_ajax_object.ajax_url,
        data: {
        action: "casino_listing_load_more_casinos",
        page: toplistPage,
      },

      beforeSend: function () {
        button.hide();
      },

      success: function (data) {
 
        $('.load-more-items').append(data);
        button.show();
        $('.list-items.load-more-items .element-1 span').each(function(index) {
          console.log( index++ + $( this ).text( ) );
          $( this ).text( '#' + index );
        })
        if($.trim(data).length == 0)  {
          button.hide();
        } 
      },
    
      complete: function (data) {
        
      },

      error: function (data) {
      }
    });
  });
});
})(jQuery);