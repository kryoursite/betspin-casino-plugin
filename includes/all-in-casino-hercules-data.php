<?php

//Details about casino + for shortcodes
$shortname = get_field('casino_shortname');
$name = do_shortcode("[media data=fullname casino=$shortname type=livecasino]");
$logo = do_shortcode("[media data=logo casino=$shortname type=livecasino]");
$ribbon = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon type=livecasino]");
$oneliner = do_shortcode("[media data=oneliner casino=$shortname type=livecasino]");
$pro1 = do_shortcode("[media data=extra-fields casino=$shortname field=pro1 type=livecasino]");
$pro2 = do_shortcode("[media data=extra-fields casino=$shortname field=pro2 type=livecasino]");
$pro3 = do_shortcode("[media data=extra-fields casino=$shortname field=pro3 type=livecasino]");
$slot_games = do_shortcode("[media data=extra-fields casino=$shortname field=slot_games type=livecasino]");
$live_games = do_shortcode("[media data=extra-fields casino=$shortname field=live_games type=livecasino]");
$rib_col = do_shortcode("[media data=extra-fields casino=$shortname field=ribbon_color type=livecasino]");
$bonus_code = do_shortcode("[media data=extra-fields casino=$shortname field=bonus-code type=livecasino]");

//Trackers(affiliate links)
$uk_tracker = do_shortcode("[media data=tracker casino=$shortname type=livecasino]");
$ca_tracker = do_shortcode("[media data=tracker casino=$shortname tracker=ca type=livecasino]");

//If tnc enabled
$tnc_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_text_enabled type=livecasino]");
$tnc = do_shortcode("[media data=extra-fields casino=$shortname field=tnc-text type=livecasino]");

//If tnc link enabled
$tnc_link = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_condition_link_text type=livecasino]");
$tnc_link_enabled = do_shortcode("[media data=extra-fields casino=$shortname field=terms_and_conditions_link_enabled type=livecasino]");
