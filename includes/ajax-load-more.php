<?php

add_action('wp_ajax_nopriv_casino_listing_load_more_casinos', 'casino_listing_load_more_casinos');
add_action('wp_ajax_casino_listing_load_more_casinos', 'casino_listing_load_more_casinos');


function casino_listing_load_more_casinos()
{

    $pageNumber = (int)$_POST["page"];            
    $page = empty( $pageNumber )? 1 : $pageNumber;
    $postPerPage = (int)$_POST["ppp"];     
    $ppp = empty($postPerPage) ? 10 : $postPerPage; 

                $atts = array(
                    'post_type' => 'casino-review',
                    'orderby' => 'title',
                    'order' => 'ASC',
                    'posts_per_page' => $ppp,
                    'paged' => $page,
                    'type' => 'livecasino',
                    'orderby' => 'post_name__in',
                    'meta_key' => 'aic_archive_page',
                    'meta_value' => 0,
                );

    $loop = new WP_Query($atts);
    while ($loop->have_posts()) :
        $loop->the_post();
        include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/top-three-shortcode.php';
    // End the loop.
    endwhile;

    wp_reset_postdata();

    die();
}
