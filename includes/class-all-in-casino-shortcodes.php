<?php
// If this file is called directly, abort.
if (!defined('ABSPATH')) {
    die;
}
/**
 * Plugin Shortcode Functionality
 *
 */
if (!class_exists('All_In_Casino_Shortcodes')) :
    class All_In_Casino_Shortcodes
    {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * @var it will be all the css for all shortcodes
         */
        protected $shortcode_css;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         *
         * @param      string $plugin_name The name of the plugin.
         * @param      string $version The version of this plugin.
         */
        public function __construct($plugin_name, $version)
        {

            $this->plugin_name = $plugin_name;
            $this->version     = $version;
            $this->setup_hooks();
        }

        public function setup_hooks()
        {
            //add_action('wp_enqueue_scripts', array($this, 'register_style'));

            // add_action('get_footer', array($this, 'enqueue_scripts'));
        }

        public function register_style()
        {
            //wp_enqueue_style($this->plugin_name . '-shortcodes', ALL_IN_CASINO_PLUGIN_URL . 'public/css/all-in-casino-shortcodes.css', array(), $this->version, 'all');
        }

        public function casino_top3($atts)
        {
            $atts = shortcode_atts(
                array(
                    'reviews' => '',
                    'country' => '',
                    'disclosure' => 'on',
                    'limit' => 10,
                    'payments' => '',
                    'itemlist' => 'on',
                    'filter' => '',
                    'games' => '',
                    'load-more' =>'',
                    'type' => 'livecasino'
                ),
                $atts,
                'casino_top3'
            );


            if ($atts['load-more'] == "on") {
                $loadMoreButton = "<div class='load-more-wrap'><span class='load-more-casinos'>Load more</span></div>";
                $loadMoreItems = "load-more-items";
            } else {
                $loadMoreButton = "";
                $loadMoreItems = "";
            }

            $slugs = $atts['reviews'];
            $slugs = explode(',', $slugs);
            $payments = $atts['payments'];
            $games = $atts['games'];

            $loop_args = array(
                'posts_per_page' => $atts['limit'],
                'post_type' => 'casino-review',
                'orderby' => 'post_name__in',
                'meta_key' => 'aic_archive_page',
                'meta_value' => 0,
            );

            if (!empty($payments)) {
                $loop_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'payment-method',
                        'field' => 'slug',
                        'terms' => $payments,
                    ),
                );
            }
            if (!empty($games)) {
                $loop_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'games-buttons',
                        'field' => 'slug',
                        'terms' => $games,
                    ),
                );
            }

            if (!empty($atts['reviews'])) {
                $loop_args['post_name__in'] = $slugs;
            }

            $loop = new WP_Query($loop_args);

            ob_start();
?>
            <div class="top-three-list">
            <div id='loader' style="display: none;">
                    <img src=<?php echo ALL_IN_CASINO_PLUGIN_URL . 'public/img/reload.gif'; ?>>
                </div>
                    <div class="list-items <?php echo $loadMoreItems ?>" <?php if ($atts['itemlist'] == 'on') : echo 'itemscope itemtype="https://schema.org/ItemList"';
                            endif; ?>>
                <?php
                $num = 0;
                while ($loop->have_posts()) :
                    $num++;
                    $loop->the_post();
                    include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/top-three-shortcode.php';
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
            </div>
            <?php echo $loadMoreButton ?>

            <?php if (get_field('disclosure_text', 'options') && $atts['disclosure'] === "on") : ?>
                <div class="disclosure"><span class="disc-hover">&#8505; Advertiser Disclosure</span>
                    <p class="disclosure-text">
                        <?php the_field('disclosure_text', 'options'); ?>
                    </p>
                </div>
            <?php endif; ?>
        <?php
            return ob_get_clean();
        }

        public function casino_box($atts)
        {
            $atts = shortcode_atts(
                array(
                    'reviews' => '',
                    'country' => '',
                    'disclosure' => 'on',
                    'limit' => 10,
                    'payments' => '',
                    'games' => '',

                ),
                $atts,
                'casino_box'
            );

            $slugs = $atts['reviews'];
            $slugs = explode(',', $slugs);
            $payments = $atts['payments'];
            $games = $atts['games'];

            $loop_args = array(
                'posts_per_page' => $atts['limit'],
                'post_type' => 'casino-review',
                'orderby' => 'post_name__in',
                'meta_key' => 'aic_archive_page',
                'meta_value' => 0,
            );

            if (!empty($payments)) {
                $loop_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'payment-method',
                        'field' => 'slug',
                        'terms' => $payments,
                    ),
                );
            }
            if (!empty($games)) {
                $loop_args['tax_query'] = array(
                    array(
                        'taxonomy' => 'games-buttons',
                        'field' => 'slug',
                        'terms' => $games,
                    ),
                );
            }

            if (!empty($atts['reviews'])) {
                $loop_args['post_name__in'] = $slugs;
            }

            $loop = new WP_Query($loop_args);

            ob_start();
        ?>

            <div class="box-list">
                <?php
                while ($loop->have_posts()) :

                    $loop->the_post();

                    include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/casino-box-shortcode.php';

                endwhile;
                wp_reset_postdata();
                ?>
            </div>
            <?php if (get_field('disclosure_text', 'options') && $atts['disclosure'] === "on") : ?>
                <div class="disclosure"><span class="disc-hover">&#8505; Advertiser Disclosure</span>
                    <p class="disclosure-text">
                        <?php the_field('disclosure_text', 'options'); ?>
                    </p>
                </div>
            <?php endif; ?>
        <?php
            return ob_get_clean();
        }

        public function videoslot($atts)
        {
            $atts = shortcode_atts(
                array(
                    'reviews' => '',
                    'filter' => '',
                    'limit' => -1,
                ),
                $atts,
                'videoslot'
            );

            $filter = $atts['filter'];

            $slugs = $atts['reviews'];
            $slugs = explode(',', $slugs);

            $loop_args = array(
                'post_type' => 'videoslot',
                'orderby' => 'post_name__in',
                'posts_per_page' => $atts['limit'],
            );

            if (!empty($atts['reviews'])) {
                $loop_args['post_name__in'] = $slugs;
            }

            $loop = new WP_Query($loop_args);

            ob_start();

            $taxonomies = get_terms(array(
                'taxonomy' => 'software',
                'hide_empty' => true
            ));

            if (!empty($taxonomies)) :
                $output = "<div class='videoslot-filter'>";

                foreach ($taxonomies as $category) {
                    $term_id = $category->term_id;
                    $output .= "<a data-category='{$term_id}' class='vs-js-filter-item vs-button-filter' href='#{$category->slug}'>$category->slug</a>";
                }
                $output .= "</div>";
                echo $output;
            endif;

        ?>
            <div class="vs-loader">
                <div class="lds-ripple">
                    <div></div>
                    <div></div>
                </div>
            </div>

            <div class="videoslot-list vs-js-filter">

                <?php

                while ($loop->have_posts()) :

                    $loop->the_post();

                    include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/videoslot-shortcode.php';

                endwhile;
                wp_reset_postdata();
                ?>
            </div>

        <?php
            return ob_get_clean();
        }

        public function related_videoslot($atts)
        {
            $atts = shortcode_atts(
                array(
                    'limit' => 7,
                    'tax' => '',
                ),
                $atts,
                'related_videoslot'
            );

            $loop_args = array(
                'post_type' => 'videoslot',
                'posts_per_page' => $atts['limit'],
                'tax_query' => array(
                    array(
                        'taxonomy' => 'software',
                        'field'    => 'term_id',
                        'terms'    => $atts['tax'],
                    ),
                ),

            );

            $loop = new WP_Query($loop_args);

            ob_start();
        ?>

            <div class="videoslot-list">

                <?php

                while ($loop->have_posts()) :

                    $loop->the_post();

                    include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/videoslot-shortcode.php';

                endwhile;
                wp_reset_postdata();
                ?>
            </div>

        <?php
            return ob_get_clean();
        }


        public function iframe_videoslot($atts)
        {
            $atts = shortcode_atts(
                array(
                    'limit' => 5,
                    'tax' => '',
                ),
                $atts,
                'iframe_videoslot'
            );

            $loop_args = array(
                'post_type' => 'videoslot',
                'posts_per_page' => $atts['limit'],
                'tax_query' => array(
                    array(
                        'taxonomy' => 'software',
                        'field'    => 'term_id',
                        'terms'    => $atts['tax'],
                    ),
                ),

            );

            $loop = new WP_Query($loop_args);

            ob_start();
        ?>

            <div class="iframe-vs-list">

                <?php

                while ($loop->have_posts()) :

                    $loop->the_post();

                    include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/iframe-shortcode.php';

                endwhile;
                wp_reset_postdata();
                ?>
            </div>

<?php
            return ob_get_clean();
        }
    }
endif;
