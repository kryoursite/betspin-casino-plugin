<?php

class All_In_Casino_Widget_Payment_Methods extends WP_Widget
{

    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        $widget_ops = array(
            'classname' => '',
            'description' => 'Display Payment Methods',
        );
        parent::__construct('payment_methods', 'Payment Methods', $widget_ops);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $widget_id = $args['widget_id'];
        // $reviews = get_field('review_widget_post_object', 'widget_' . $widget_id);

        echo '<div class="casino-review-widget">';
        echo $args['before_title'];
        //Widget Title
        echo get_field('payment_widget_header', 'widget_' . $widget_id);
        echo $args['after_title'];

        include ALL_IN_CASINO_BASE_DIR . 'templates/casino-reviews/widgets/payment-methods-widget.php';

        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form($instance)
    {
        // outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved
    }
}
