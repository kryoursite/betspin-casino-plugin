<?php

add_action('wp_ajax_nopriv_filter', 'filter_ajax');
add_action('wp_ajax_filter', 'filter_ajax');

add_action('wp_ajax_nopriv_filter_vs_ajax', 'filter_vs_ajax');
add_action('wp_ajax_filter_vs_ajax', 'filter_vs_ajax');


function filter_ajax()
{
    $category = $_POST['category'];

    $loop_args = array(
        'post_type' => 'casino-review',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'payment-method',
                'field'    => 'term_id',
                'terms'    => $category,
                'operator' => 'IN'
            )
        ),

    );

    $loop = new WP_Query($loop_args);
    while ($loop->have_posts()) :
        $loop->the_post();
        include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/casino-simple-shortcode.php';
    // End the loop.
    endwhile;
    wp_reset_postdata();

    die();
}


function filter_vs_ajax()
{
    $category = $_POST['category'];

    $loop_args = array(
        'post_type' => 'videoslot',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'software',
                'field'    => 'term_id',
                'terms'    => $category,
                'operator' => 'IN'
            )
        ),
    );

    $loop = new WP_Query($loop_args);
    while ($loop->have_posts()) :
        $loop->the_post();
        include ALL_IN_CASINO_BASE_DIR . 'templates/shortcodes/videoslot-shortcode.php';
    // End the loop.
    endwhile;
    wp_reset_postdata();

    die();
}