<?php

/**
 *
 * @link       yoursite.lv
 * @since      1.0.0
 *
 * @package    All_In_Casino
 * @subpackage All_In_Casino/public
 */

/**
 * Functionality for our custom post types
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    All_In_Casino
 * @subpackage All_In_Casino/public
 * @author     KR <yoursite.lv>
 */
class All_In_Casino_Post_Types
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    public function init()
    {
        $this->register_cpt_casino_review();
        $this->register_cpt_casino_tax();
        $this->register_cpt_games_tax();
        $this->register_cpt_videoslot();
        $this->register_cpt_videoslot_tax();
    }

    public function register_cpt_casino_review()
    {
        $labels = array(
            'name'                  => _x('Casino Reviews', 'Post type general name', 'all-in-casino'),
            'singular_name'         => _x('Live Casinos', 'Post type singular name', 'all-in-casino'),
            'menu_name'             => _x('Casino Reviews', 'Admin Menu text', 'all-in-casino'),
            'name_admin_bar'        => _x('Casino Review', 'Add New on Toolbar', 'all-in-casino'),
            'add_new'               => __('Add New', 'all-in-casino'),
            'add_new_item'          => __('Add New Casino Review', 'all-in-casino'),
            'new_item'              => __('New Casino Review', 'all-in-casino'),
            'edit_item'             => __('Edit Casino Review', 'all-in-casino'),
            'view_item'             => __('View Casino Review', 'all-in-casino'),
            'all_items'             => __('All Casino Reviews', 'all-in-casino'),
            'search_items'          => __('Search Casino Reviews', 'all-in-casino'),
            'parent_item_colon'     => __('Parent Casino Reviews:', 'all-in-casino'),
            'not_found'             => __('No casino reviews found.', 'all-in-casino'),
            'not_found_in_trash'    => __('No casino reviews found in Trash.', 'all-in-casino'),
            'featured_image'        => _x('Casino review Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'all-in-casino'),
            'set_featured_image'    => _x('Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'all-in-casino'),
            'remove_featured_image' => _x('Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'all-in-casino'),
            'use_featured_image'    => _x('Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'all-in-casino'),
            'archives'              => _x('Casino review archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'all-in-casino'),
            'insert_into_item'      => _x('Insert into casino review', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'all-in-casino'),
            'uploaded_to_this_item' => _x('Uploaded to this casino review', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'all-in-casino'),
            'filter_items_list'     => _x('Filter casino reviews list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'all-in-casino'),
            'items_list_navigation' => _x('Casino reviews list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'all-in-casino'),
            'items_list'            => _x('Casino reviews list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'all-in-casino'),
        );

        $args = array(
            'labels' => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'live-casinos'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-text-page',
            'supports'           => array('title', 'editor', 'author', 'thumbnail'),
        );

        register_post_type('casino-review', $args);
    }

    public function register_cpt_casino_tax()
    {
        $labels = [
            'name'              => _x('Payment Methods', 'taxonomy general name'),
            'singular_name'     => _x('Payment Method', 'taxonomy singular name'),
            'search_items'      => __('Search Payment Methods'),
            'all_items'         => __('All Payment Methods'),
            'parent_item'       => __('Parent Payment Method'),
            'parent_item_colon' => __('Parent Payment Method:'),
            'edit_item'         => __('Edit Payment Method'),
            'update_item'       => __('Update Payment Method'),
            'add_new_item'      => __('Add New Payment Method'),
            'new_item_name'     => __('New Payment Method Name'),
            'menu_name'         => __('Payment Methods'),
        ];
        $args = [
            'hierarchical'      => true, // make it hierarchical (like categories)
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'payment-method'],
        ];
        register_taxonomy('payment-method', ['casino-review'], $args);
    }

    public function register_cpt_games_tax()
    {
        $labels = [
            'name'              => _x('Games', 'taxonomy general name'),
            'singular_name'     => _x('Game', 'taxonomy singular name'),
            'search_items'      => __('Search Game'),
            'all_items'         => __('All Games'),
            'parent_item'       => __('Parent Game'),
            'parent_item_colon' => __('Parent Game:'),
            'edit_item'         => __('Edit Game'),
            'update_item'       => __('Update Game'),
            'add_new_item'      => __('Add New Game'),
            'new_item_name'     => __('New Game Name'),
            'menu_name'         => __('Games'),
        ];
        $args = [
            'hierarchical'      => true, // make it hierarchical (like categories)
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'game-buttons'],
        ];
        register_taxonomy('games-buttons', ['casino-review'], $args);
    }
    public function register_cpt_videoslot()
    {
        $labels = array(
            'name'                  => _x('Videoslots', 'Post type general name', 'all-in-casino'),
            'singular_name'         => _x('Videoslot', 'Post type singular name', 'all-in-casino'),
            'menu_name'             => _x('Videoslots', 'Admin Menu text', 'all-in-casino'),
            'name_admin_bar'        => _x('Videoslot', 'Add New on Toolbar', 'all-in-casino'),
            'add_new'               => __('Add New', 'all-in-casino'),
            'add_new_item'          => __('Add New Casino Review', 'all-in-casino'),
            'new_item'              => __('New Videslot', 'all-in-casino'),
        );

        $args = array(
            'labels' => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'videoslot'),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-admin-collapse',
            'supports'           => array('title', 'editor', 'author', 'thumbnail'),
        );

        register_post_type('videoslot', $args);
    }

    public function register_cpt_videoslot_tax()
    {
        $labels = [
            'name'              => _x('Software', 'taxonomy general name'),
            'singular_name'     => _x('Software', 'taxonomy singular name'),
            'search_items'      => __('Search Software'),
            'all_items'         => __('All Software'),
            'parent_item'       => __('Parent Software'),
            'parent_item_colon' => __('Parent Software:'),
            'edit_item'         => __('Edit Software'),
            'update_item'       => __('Update Software'),
            'add_new_item'      => __('Add New Software'),
            'new_item_name'     => __('New Software Name'),
            'menu_name'         => __('Software'),
        ];
        $args = [
            'hierarchical'      => true, // make it hierarchical (like categories)
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'software'],
        ];
        register_taxonomy('software', ['videoslot'], $args);
    }

    public function single_template_casino_review($template)
    {
        if (is_singular('casino-review')) {
            //Template for casino CPT
            require_once ALL_IN_CASINO_BASE_DIR . 'public/class-all-in-casino-template-loader.php';

            $template_loader = new All_In_Casino_Template_Loader();

            return $template_loader->get_template_part('single', 'casino-review', false);
        }

        return $template;
    }

    public function archive_template_casino_review($template)
    {
        if (is_archive('casino-review')) {
            //Template for casino slot CPT
            require_once ALL_IN_CASINO_BASE_DIR . 'public/class-all-in-casino-template-loader.php';

            $template_loader = new All_In_Casino_Template_Loader();

            return $template_loader->get_template_part('archive', 'casino-review', false);
        }

        return $template;
    }

    public function single_template_videoslot($template)
    {
        if (is_singular('videoslot')) {
            //Template for casino slot CPT
            require_once ALL_IN_CASINO_BASE_DIR . 'public/class-all-in-casino-template-loader.php';

            $template_loader = new All_In_Casino_Template_Loader();

            return $template_loader->get_template_part('single', 'videoslot', false);
        }

        return $template;
    }
}
