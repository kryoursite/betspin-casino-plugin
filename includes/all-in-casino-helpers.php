<?php
//Custom schema for casino review archive page
function add_review_archive_page_schema()
{
    $schema = get_field('aic_review_archive_schema', 'options');
    if (!empty($schema)) {
        if (is_post_type_archive('casino-review')) {
            echo '<script type="application/ld+json">' . $schema . '</script>';
        }
    }
}

add_action('wp_head', 'add_review_archive_page_schema');


function generate_review_content_pages()
{
    $loop_args = array(
        'posts_per_page' => -1,
        'post_type' => 'casino-review',
        'meta_key' => 'country_name',
        'orderby' => 'meta_value',
        'order' => 'ASC'
    );

    $loop = new WP_Query($loop_args);

    while ($loop->have_posts()) :

        $loop->the_post();
?>
        <div class="archive-page">
            <a href="<?php the_permalink(); ?>">
                <?php if (get_field('country_name')) : ?>
                    <?php the_field('country_name'); ?>
                <?php else : ?>
                    <?php the_title(); ?>
                <?php endif; ?>
            </a>
        </div>
    <?php
    endwhile;
    wp_reset_postdata();
}

//Add custom hreflang map for selected country pages
function custom_hreflang_map()
{
    global $post;

    $ids = get_posts(array(
        'fields'          => 'ids',
        'posts_per_page'  => -1,
        'post_type' => 'casino-review'
    ));

    $cpt_link = get_post_type_archive_link('casino-review');

    if (is_post_type_archive('casino-review')) {

        echo '<link rel="canonical" href="' . $cpt_link . '">' . "\n";

        foreach ($ids as $id) {
            if (get_field('page_hreflang', $id)) {
                echo '<link rel="alternate" href="' . get_permalink($id) . '" hreflang="' . strtolower(get_field('page_hreflang', $id)) . '">' . "\n";
            }
        }

        echo "<link rel='alternate' href='{$cpt_link}' hreflang='x-default'>" . "\n";

    } else {

        if (get_field('enable_hreflang', $post->ID)) {
            
            if (get_field('page_hreflang', $post->ID)) {

                echo '<link rel="canonical" href="' . get_permalink() . '">' . "\n";

                foreach ($ids as $id) {
                    if (get_field('page_hreflang', $id)) {
                        echo '<link rel="alternate" href="' . get_permalink($id) . '" hreflang="' . strtolower(get_field('page_hreflang', $id)) . '">' . "\n";
                    }
                }

                echo "<link rel='alternate' href='{$cpt_link}' hreflang='x-default'>" . "\n";
            }
        }
    }
}

add_action('wp_head', 'custom_hreflang_map', 1);


/**
 * Allow changing of the canonical URL.
 *
 * @param string $canonical The canonical URL.
 */
/*add_filter('rank_math/frontend/canonical', function ($canonical) {
    //target a page using its page id
    if (get_field('enable_hreflang')) {
        return false;
    }
    return $canonical;
});*/

function betspin_ratings_row()
{
    $data = \MBO\Admin\Singletons\Cache::self()->get_affiliate();
    $lang = \MBO\Admin\Helpers::get_language();
    $casino = get_field('casino_shortname');
    $overall = $data->operators->$casino->$lang->livecasino->rating;

    $ratings_array = [
        "bonuses" => $data->operators->$casino->$lang->livecasino->rating_bonuses,
        "games" => $data->operators->$casino->$lang->livecasino->rating_games,
        "casino" => $data->operators->$casino->$lang->livecasino->rating_casino,
        "customer_rating" => $data->operators->$casino->$lang->livecasino->rating_customer
    ];
    ?>

    <div class="ratings-wrap">
        <div class="ratings-bars">
            <?php foreach ($ratings_array as $key => $value) : ?>
                <?php if (!empty($value)) : ?>
                    <div class="ratings-box">
                        <div class="ratings-name"><?php echo str_replace('_', ' ', $key); ?></div>
                        <div class="ratings-bar">
                            <div class="bar-background"></div>
                            <div class="bar-main" style="width:<?php echo $value * 20; ?>%"></div>
                        </div>
                        <div class="ratings-value"><span><?php echo $value; ?></span>/5</div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php if (!empty($ratings_array['bonuses']) || !empty($ratings_array['games']) || !empty($ratings_array['casino']) || !empty($ratings_array['customer_rating'])) : ?>
            <div class="ratings-circle">
                <div class="c100 <?php if (!empty($overall)) echo 'p' . $overall * 20; ?> big center">
                    <?php if (!empty($overall)) : ?>
                        <span><?php echo $overall; ?> / 5</span>
                    <?php endif; ?>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php
}
